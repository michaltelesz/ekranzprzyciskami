﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class CountdownText : MonoBehaviour
{
    public float countdownTime = 1;
    private float elapsedTime;
    
    void Start()
    {
        elapsedTime = 0;
    }

    void Update()
    {
        elapsedTime += Time.deltaTime;
        if (elapsedTime >= countdownTime)
        {
            Destroy(gameObject);
        }
    }
}
