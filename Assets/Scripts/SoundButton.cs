﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SoundButton : MonoBehaviour
{
    private SoundSwitch[] _soundSwitches;
    private SoundSwitch _lastSwitch;

    // Start is called before the first frame update
    void Start()
    {
        _soundSwitches = transform.parent.GetComponentsInChildren<SoundSwitch>();
    }

    public void PlaySound()
    {
        if (_lastSwitch == null || !_lastSwitch.IsPlaying)
        {
            IList<SoundSwitch> activeSwitches = _soundSwitches.Where(s => s.IsActive).ToList();
            if (activeSwitches.Count > 0)
            {
                int randomIndex = Random.Range(0, activeSwitches.Count);
                _lastSwitch = activeSwitches[randomIndex];
                _lastSwitch.PlaySound();
            }
        }
    }
}