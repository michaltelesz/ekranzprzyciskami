﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SoundSwitch : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Color colorOn;
    [SerializeField] private Color colorOff;

    [SerializeField] private Image dotImage;

    private bool _isActive;
    private Vector2 _leftDotPosition;
    private Vector2 _rightDotPosition;
    private RectTransform _dotRectTransform;
    private AudioSource _audioSource;

    public bool IsActive => _isActive;

    public bool IsPlaying => _audioSource.isPlaying;

    private void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        _dotRectTransform = dotImage.GetComponent<RectTransform>();
        if (rectTransform != null && _dotRectTransform != null)
        {
            float dotHalfWidth = _dotRectTransform.rect.width / 2f;
            float rectHalfWidth = rectTransform.rect.width / 2f;
            _leftDotPosition = new Vector2(-rectHalfWidth + dotHalfWidth, 0);
            _rightDotPosition = new Vector2(rectHalfWidth - dotHalfWidth, 0);
        }

        _audioSource = GetComponent<AudioSource>();

        Toggle(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Toggle(!_isActive);
    }

    private void Toggle(bool state)
    {
        _isActive = state;
        if (state)
        {
            dotImage.color = colorOn;
            _dotRectTransform.anchoredPosition = _rightDotPosition;
        }
        else
        {
            dotImage.color = colorOff;
            _dotRectTransform.anchoredPosition = _leftDotPosition;
        }
    }

    public void PlaySound()
    {
        _audioSource.Play();
    }
}