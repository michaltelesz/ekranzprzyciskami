﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupButton : MonoBehaviour
{
    [SerializeField] private Transform popupParent;
    [SerializeField] private Popup popupPrefab;

    public void ShowPopup()
    {
        if (popupParent != null && popupPrefab != null)
        {
            Instantiate(popupPrefab, popupParent);
        }
    }
}