﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextButton : MonoBehaviour
{
    [SerializeField] private Transform textParent;
    [SerializeField] private CountdownText textPrefab;

    public void ShowText()
    {
        if (textParent != null && textParent.childCount == 0 && textPrefab != null)
        {
            CountdownText countdownText = Instantiate(textPrefab, textParent);
            countdownText.countdownTime = 3;
        }
    }
}